<?php

/**
 * @file
 * Contains bundle and field related functionality.
 */

/* -------------------------- Country Region Bundle ------------------------- */

/**
 * Implements hook_entity_info().
 */
function countries_regions_entity_info() {
  $return = array(
    'country_region' => array(
      'label' => t('Region'),
      'base table' => 'countries_region',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'rid',
      ),
      'bundles' => array(
        'country_region' => array(
          'label' => t('Region'),
          'admin' => array(
            'path' => 'admin/config/regional/regions',
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Region'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
  return $return;
}

/**
 * Implements hook_field_extra_fields().
 */
function countries_regions_field_extra_fields() {
  $extra = array();

  $extra['country_region']['country_region'] = array(
    'form' => array(),
    'display' => array()
  );

  $core_properties = array(
    'iso2' => t('ISO alpha-2 country code'),
    'name' => t('Region name'),
    'note' => t('Region note'),
    'synonym' => t('Region synonym'),
    'code' => t('Code'),
    'addr_exclude' => t('Exclude flag'),
    'type_selector' => t('Regional type selector'),
    'type' => t('Region type'),
  );

  $weight = -20;
  foreach ($core_properties as $key => $title) {
    $extra['country_region']['country_region']['form'][$key] = array(
      'label' => $title,
      'description' => $title,
      'weight' => $weight++,
    );
  }

  return $extra;
}

/* -------------------------- Country Region Fields ------------------------- */

/**
 * Implement hook_field_info().
 */
function countries_regions_field_info() {
  return array(
    'region' => array(
      'label' => t('Region'),
      'description' => t('This field stores a region reference in the database.'),
      'settings' => array(
        'max_depth' => 1,
        'regional_types' => array(),
        'include_child_nodes' => 1,
        'countries' => array(),
        'continents' => array(),
        'enabled' => COUNTRIES_ENABLED,
      ),
      'default_widget' => 'options_select',
      'default_formatter' => 'region_default',
    ),
  );
}

/**
 * Implement hook_field_settings_form().
 */
function countries_regions_field_settings_form($field, $instance, $has_data) {
  $form = array();
  $settings = $field['settings'];
  $form['max_depth'] = array(
    '#type' => 'select',
    '#title' => t('Maximum depth'),
    '#default_value' => isset($settings['max_depth']) ? $settings['max_depth'] : COUNTRIES_REGIONS_MAX_DEPTH,
    '#options' => drupal_map_assoc(range(1, COUNTRIES_REGIONS_MAX_DEPTH, 1)),
    '#description' => t('Defines how deep in the regional tree the field should traverse when searching for regions. If you set this value to small you will may not find deeper regions that you actually want.'),
  );

  $form['regional_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Filter by regional types'),
    '#default_value' => isset($settings['regional_types']) ? $settings['regional_types'] : array(),
    '#options' => countries_regional_types(),
    '#description' => t('If no regional types are selected, this filter will not be used.'),
    '#element_validate' => array('_countries_checkbox_filter'),
  );

  $form['include_child_nodes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include leaf regions'),
    '#default_value' => isset($settings['include_child_nodes']) ? $settings['include_child_nodes'] : 1,
    '#description' => t('Leaf regions refer to the last or deepest regions from the regional tree.'),
  );

  countries_filter_options_form($form, $settings);
  return $form;
}

/**
 * Implement hook_field_load().
 */
function countries_regions_field_load($obj_type, $objects, $field, $instances, $langcode, &$items) {
  foreach ($objects as $id => $object) {
    foreach ($items[$id] as $delta => $item) {
      $items[$id][$delta]['safe'] = implode(', ', countries_regions_parental_path($item['rid']));
    }
  }
}

/**
 * Implement hook_field_is_empty().
 */
function countries_regions_field_is_empty($item, $field) {
  return empty($item['rid']);
}

/**
 * Implements hook_field_formatter_info().
 */
function countries_regions_field_formatter_info() {
  $formatters = array(
    'region_default' => array(
      'label' => t('Default'),
      'field types' => array('region'),
    ),
    'region_region' => array(
      'label' => t('Region'),
      'field types' => array('region'),
    ),
    'region_country' => array(
      'label' => t('Region and country'),
      'field types' => array('region'),
    ),
    'region_country_only' => array(
      'label' => t('Country'),
      'field types' => array('region'),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_view().
 */
function countries_regions_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  foreach ($items as $delta => $item) {
    $rid = $item['rid'];
    switch ($display['type']) {
      case 'region_default':
        $element[$delta]['#markup'] = $item['safe'];
        break;

      case 'region_region':
        if ($region = countries_regions_load($rid)) {
          $element[$delta]['#markup'] = check_plain($region->name);
        }
        break;

      case 'region_country':
        if ($region = countries_regions_load($rid)) {
          if ($country = countries_get_country($region->iso2)) {
            $element[$delta]['#markup'] = t('@region, @country', array('@region' => $region->name, '@country' => $country->name));
          }
          else {
            $element[$delta]['#markup'] = check_plain($region->name);
          }
        }
        break;

      case 'region_country_only':
        if ($region = countries_regions_load($rid)) {
          if ($country = countries_get_country($region->iso2)) {
            $element[$delta]['#markup'] = check_plain($country->name);
          }
        }
        break;


    }
  }

  return $element;
}

/**
 * Implements hook_field_widget_info_alter().
 */
function countries_regions_field_widget_info_alter(&$info) {
  $info['options_buttons']['field types'][] = 'region';
  $info['options_select']['field types'][] = 'region';
}

/**
 * Implements hook_options_list().
 */
function countries_regions_options_list($field) {
  $function = !empty($field['settings']['options_list_callback']) ? $field['settings']['options_list_callback'] : 'countries_regions_field_options';

  $filters = array(
    'continents' => array_filter($field['settings']['continents']),
    'enabled' => $field['settings']['enabled'],
  );

  if (!isset($field['#filters'])) {
    $field['#filters'] = $filters;
  }
  return $function($field);
}

function countries_regions_options($tree, $indent = '-- ', $depth = 0) {
  $regions = array();
  foreach ($tree as $region) {
    $regions[$region->rid] = $indent . $region->name;
    if (!empty($region->subregions)) {
      $regions += countries_regions_options($region->subregions, $indent . '-- ', $depth + 1);
    }
  }
  return $regions;
}

/**
 * Our process callback to expand the control.
 */
function countries_regions_field_options($element) {
  $settings['max_depth'] = empty($element['settings']['max_depth']) ? COUNTRIES_REGIONS_MAX_DEPTH : $element['settings']['max_depth'];
  // todo depth
  $settings['include_child_nodes'] = isset($element['settings']['include_child_nodes']) ? $element['settings']['include_child_nodes'] : 1;

  $settings['filters'] = array_intersect_key($element['settings'], array_flip(array('enabled', 'continents', 'countries')));
  if (!empty($element['settings']['regional_types'])) {
    $settings['filters']['regional_types'] = $element['settings']['regional_types'];
  }

  $tree = countries_regions_tree(NULL, NULL, $settings);
  $options = array();
  $multiple_countries = count($tree) > 1;
  foreach ($tree as $iso2 => $info) {
    if ($multiple_countries) {
      $options[$info->name] = countries_regions_options($info->subregions);
    }
    else {
      return countries_regions_options($info->subregions, '');
    }
  }
  return $options;
}
