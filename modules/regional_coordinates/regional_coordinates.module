<?php

/**
 * Implements hook_countries_configuration_form_ID_alter()
 */
function regional_coordinates_countries_configuration_form_countries_regions_alter(&$subform, $values, &$addtional) {
  // These are also passed by to this function, however these are not needed
  // with this implementation.
  $form_state = &$addtional['form_state'];
  $form = &$addtional['form'];
  $country = $addtional['country'];

  $values = $values['regional_coordinates'];

  $subform['regional_coordinates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default coordinates'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $subform['regional_coordinates']['coordinates'] = array(
    '#type' => 'point',
    '#title' => t('Default coordinate when creating a region in the country'),
    '#default_value' => $values['coordinates'],
  );
  $subform['regional_coordinates']['bounding_box'] = array();
  $subform['regional_coordinates']['bounding_box']['topleft'] = array(
    '#title' => t('Top left coordinates for the country.'),
    '#type' => 'point',
    '#default_value' => $values['bounding_box']['topleft'],
  );
  $subform['regional_coordinates']['bounding_box']['bottomright'] = array(
    '#title' => t('Bottom right coordinates for the country.'),
    '#type' => 'point',
    '#default_value' => $values['bounding_box']['bottomright'],
  );
}

/**
 * Helper function to get the default country coordinates.
 *
 * Additional coordinates
 * AP, EU
 *
 * todo:
 * AX Aland Islands
 * AD Andorra
 * BQ Bonaire, Sint Eustatius and Saba
 * CW Curaçao
 * GG Guernsey
 * IM Isle of Man
 * JE Jersey
 * PN Pitcairn
 * BL Saint Barthélemy
 * MF Saint Martin (French part)
 * SX Sint Maarten (Dutch part)
 * SS South Sudan
 * TL Timor-Leste
 */
function _regional_coordinates_country_defaults() {
  $coordinates = array();
  $handle = fopen(dirname(__FILE__) . '/regional_coordinates.csv', 'r');
  $headers = fgetcsv($handle, 1024, ",");
  while (($row = fgetcsv($handle, 1024, ",")) !== FALSE) {
    $row[1] = trim($row[1]);
    $row[2] = empty($row[2]) || $row[2] == 'NULL' ? '' : trim($row[2]); // name
    if (!empty($row[0])) {
      $coordinates[$row[0]] = array(
        'latitude' => $row[1],
        'longitude' => $row[2],
      );
    }
  }
  fclose($handle);
  return $coordinates;
}

/**
 * Implements hook_countries_configuration_options_alter().
 */
function regional_coordinates_countries_configuration_options_alter(&$values, $name, $info = array()) {
  $iso2 = is_object($info['country']) ? $info['country']->iso2 : $info['country'];
  if ($name == 'countries_regions' && $info['is_new']) {
    $values['regional_coordinates']['coordinates'] = array('latitude' => 0, 'longitude' => 0);
    $country_coordinates = _regional_coordinates_country_defaults();
    if (isset($country_coordinates[$iso2])) {
      $values['regional_coordinates']['coordinates'] = $country_coordinates[$iso2];
    }
    $values['regional_coordinates']['bounding_box'] = array(
      'topleft' => array('latitude' => 0, 'longitude' => 0),
      'bottomright' => array('latitude' => 0, 'longitude' => 0),
    );

  }
}

/**
 * Implement hook_element_info().
 *
 * Provides a simple coordinate FAPI element. Sadly, other modules do not
 * provide a FAPI level element currently.
 */
function regional_coordinates_element_info() {
  $type['point'] = array(
    '#input' => TRUE,
    '#process' => array('regional_coordinates_point_element_process', 'ajax_process_form'),
    '#element_validate' => array('regional_coordinates_point_element_validate'),
    '#theme_wrappers' => array('form_element'),
    '#format' => 'decimal',
  );
  return $type;
}

/**
 * A #process handler for the latlon element.
 */
function regional_coordinates_point_element_process($element, &$form_state, $complete_form) {
  if (empty($element['#default_value'])) {
    $element['#default_value'] = array(
      'latitude' => '',
      'longitude' => '',
    );
  }
  $value = _regional_coordinates_trim($element['#default_value']);
  $element['#field_prefix'] = '<div class="clearfix">';
  $element['#field_suffix'] = '</div>';
  $element['latitude'] = array(
    '#type' => 'textfield',
    '#description' => t('Latitude'),
    '#default_value' => $value['latitude'],
    '#required' => $element['#required'],
    '#size' => 12,
    '#maxlength' => 15,
    '#prefix' => '<div style="float:left; margin-right: 1em;">',
    '#suffix' => '</div>',
  );
  $element['longitude'] = array(
    '#type' => 'textfield',
    '#description' => t('Longitude'),
    '#default_value' => $value['longitude'],
    '#required' => $element['#required'],
    '#size' => 12,
    '#maxlength' => 15,
    '#prefix' => '<div style="float:left;">',
    '#suffix' => '</div>',
  );
  return $element;
}

/**
 * FAPI element validate callback.
 */
function regional_coordinates_point_element_validate($element, &$form_state) {
  if (!empty($element['latitude']['#value'])) {
    if (!is_numeric($element['latitude']['#value']) || $element['latitude']['#value'] > 90 || $element['latitude']['#value'] < -90) {
      form_error($element['latitude'], t('%name field latitude must be a value between -90 and 90.', array('%name' => $element['#title'])));
    }
  }
  if (!empty($element['longitude']['#value'])) {
    if (!is_numeric($element['longitude']['#value']) || $element['longitude']['#value'] > 180 || $element['longitude']['#value'] < -180) {
      form_error($element['longitude'], t('%name field longitude must be a value between -180 and 180.', array('%name' => $element['#title'])));
    }
  }
  $lon_empty = !strlen($element['longitude']['#value']);
  $lat_empty = !strlen($element['latitude']['#value']);
  if ($lon_empty xor $lat_empty) {
    if ($lon_empty) {
      $elm = $element['longitude'];
    }
    else {
      $elm = $element['latitude'];
    }
    form_error($elm, t('%name field requires both latitude and longitude values.', array('%name' => $element['#title'])));
  }
  elseif ($lon_empty && $lat_empty) {
    form_set_value($element['latitude'], 0, $form_state);
    form_set_value($element['longitude'], 0, $form_state);
  }
}

/**
 * Recursively traverses a regional relationship to find the first set of
 * coordinates for a location.
 *
 * @param object $region
 *   The region to locate coordinates for.
 *
 * @param int $depth
 *   For internal use to limit the maximun depth that it should recursive before
 *   falling back to the country coordinates.
 */
function regional_coordinates($region, $depth = 0) {
  if (!_regional_coordinates_empty($region->latitude,$region->longitude)) {
    return array(
      'latitude' => $region->latitude,
      'longitude' => $region->longitude,
    );
  }
  if ($depth < COUNTRIES_REGIONS_MAX_DEPTH && $parent = countries_regions_load($region->prid)) {
    return regional_coordinates($parent, $depth + 1);
  }
  else {
    $country = countries_get_country($region->iso2);
    $settings = countries_configuration($country, 'countries_regions');
    if (isset($settings['coordinates']['latitude'])) {
      return array(
        'latitude' => $settings['coordinates']['latitude'],
        'longitude' => $settings['coordinates']['longitude'],
      );
    }
  }
}

/**
 * Adds the regions coordinate FAPI element.
 */
function regional_coordinates_form_countries_regions_admin_form_alter(&$form, &$form_state, $form_id = NULL) {
  $coordinates = array(
    'latitude' => '',
    'longitude' => '',
  );
  if (empty($form['#region'])) {
    if (!empty($form['#config']['regional_coordinates'])) {
      $coordinates = array(
        'latitude' => $form['#config']['regional_coordinates']['coordinates']['latitude'],
        'longitude' => $form['#config']['regional_coordinates']['coordinates']['longitude'],
      );
    }
  }
  elseif (!_regional_coordinates_empty($form['#region']->latitude, $form['#region']->longitude)) {
    $region = $form['#region'];
    $coordinates = array(
      'latitude' => $form['#region']->latitude,
      'longitude' => $form['#region']->longitude,
    );
  }
  $form['regional_coordinates'] = array(
    '#type' => 'point',
    '#title' => t('Coordinates'),
    '#default_value' => $coordinates,
  );
}

/**
 * Small helper function to right trim zeros safely.
 */
function _regional_coordinates_trim($value) {
  $value['latitude'] = preg_replace('/^(.*\.\d*(?<!0)0)0*$/', '$1', $value['latitude']);
  $value['longitude'] = preg_replace('/^(.*\.\d*(?<!0)0)0*$/', '$1', $value['longitude']);
  return $value;
}

/**
 * This is a safety margin for dealing with floats in PHP.
 */
function _regional_coordinates_empty($latitude, $longitude) {
  return (max(array(abs((int)$latitude), abs((int)$longitude))) < 0.0001);
}

///**
// * Implements hook_geonames_country_import_alter().
// *
// * Currently there are no coordinates in this import!
// */
//function regional_coordinates_geonames_country_import_alter(&$country, $pieces, &$additional) {
//}
//
///**
// * Implements hook_geonames_region_import_alter().
// */
//function regional_coordinates_geonames_region_import_alter(&$country, $pieces, &$additional) {
//}
