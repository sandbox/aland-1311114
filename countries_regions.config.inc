<?php

/**
 * @file
 * Handles all of the regional settings hook_countries_configuration_options()
 * functionality for storing country specific data.
 */

/**
 * Callback for hook_countries_configuration_options().
 */
function _countries_regions_countries_configuration_options() {
  $default_labels = countries_regional_type_labels();

  $items = array(
    'countries_regions' => array(
      'title' => t('Regional settings'),
      'form callback' => 'countries_regions_country_admin_form',
      'title callback' => 'countries_regions_country_admin_form_title',
      'default values' => array(
        'regions' => array(
          0 => array(
            'label' => $default_labels[COUNTRIES_REGIONS_C3],
            'type' => COUNTRIES_REGIONS_C3,
            'exposed' => 1,
          ),
          1 => array(
            'label' => $default_labels[COUNTRIES_REGIONS_C4],
            'type' => COUNTRIES_REGIONS_C4,
            'exposed' => 1,
          ),
          2 => array(
            'label' => $default_labels[COUNTRIES_REGIONS_C5],
            'type' => COUNTRIES_REGIONS_C5,
            'exposed' => 1,
          ),
          3 => array(
            'label' => $default_labels[COUNTRIES_REGIONS_C7],
            'type' => COUNTRIES_REGIONS_C7,
            'exposed' => 1,
          ),
        ),
        'expose_country' => '',
        'expose_continent' => '',
        'max_depth' => 4,
        'rendering_depth' => 2,
        'subtypes' => array(
        ),
        'floating_subtypes' => array(
        ),
      ),
      'file' => 'countries_regions.config.inc',
      // Optional: Provides a help section to forms.
      'help' => t('This form allows you to set country specific regional defaults. Leave these blank to use the field defaults.'),
    ),
  );

  return $items;
}

/**
 * Callback for hook_countries_configuration_options().
 */
function countries_regions_country_admin_form($country, $values, &$form) {
  $options = array('' => t('-- Type --')) + countries_regional_types();

  // This is dynamically calculated on submit.
  $subform['max_depth'] = array(
    '#type' => 'value',
    '#value' => $values['max_depth'],
  );
  $subform['regions'] = array(
    '#tree' => TRUE,
    '#theme' => 'countries_regions_country_admin_form',
  );
  for ($i = 0; $i < COUNTRIES_REGIONS_MAX_DEPTH; $i++) {
    $level_values = empty($values['regions'][$i]) ? array('label' => '', 'type' => '') : $values['regions'][$i];
    $subform['regions'][$i]['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => $level_values['label'],
    );
    $subform['regions'][$i]['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#default_value' => $level_values['type'],
      '#options' => $options,
    );
    $subform['regions'][$i]['exposed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Exposed'),
      '#default_value' => empty($level_values['exposed']) ? 0 : 1,
    );
  }
  $subform['expose_country'] = array(
    '#type' => 'select',
    '#title' => t('Expose country in regional paths'),
    '#description' => t('Append country to regional paths that use the exposed feature'),
    '#default_value' => $values['expose_country'],
    '#options' => array(
      '' => t('Do not append'),
      'name' => t('Append name'),
      'official_name' => t('Append official name'),
      'iso2' => t('Append ISO alpha-2 code'),
      'iso3' => t('Append ISO alpha-3 Code'),
      'numcode' => t('Append ISO numeric Code'),
    ),
  );
  $subform['expose_continent'] = array(
    '#type' => 'select',
    '#title' => t('Expose continent in regional paths'),
    '#description' => t('Append continent to regional paths that use the exposed feature'),
    '#default_value' => $values['expose_continent'],
    '#options' => array(
      '' => t('Do not append'),
      'name' => t('Append name'),
      'code' => t('Append code'),
    ),
  );

  $rendering_depth_options = array('' => t('Complete')) + drupal_map_assoc(range(1, COUNTRIES_REGIONS_MAX_DEPTH, 1));
  $subform['rendering_depth'] = array(
    '#type' => 'select',
    '#title' => t('Admin regional tree depth'),
    '#default_value' => $values['rendering_depth'],
    '#options' => $rendering_depth_options,
    '#description' => t('This defines the depth of child regions to render on the Admin regional listing. For many countries, rendering the full regional tree would cause performance issues on this page.'),
  );

  $query = db_select('countries_regional_subtype', 'st');
  $query->fields('st');
  $query->orderBy('st.name', 'ASC');
  $options = array();
  $floating_options = array();
  foreach ($query->execute() as $type) {
    switch ($type->behaviour) {
      case 0:
        $options[$type->stid] = $type->name;
        break;
      case 1:
        $floating_options[$type->stid] = $type->name;
        break;
      case 2:
        $floating_options[$type->stid] = $type->name;
        $options[$type->stid] = $type->name;
        break;
    }

  }
  $subform['floating_subtypes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional regional types'),
    '#description' => t('These are additional floating regional types that can be assigned to regions. Floating types are points or areas that do not follow the strict tree that regions and towns usually have. For example, lakes and islands could belong to either multiple districts or be confined to a singular neibourhood.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  foreach ($floating_options as $key => $label) {
    $subform['floating_subtypes'][$key] = array(
      '#type' => 'checkbox',
      '#title' => $label,
      '#default_value' => empty($values['floating_subtypes'][$key]) ? 0 : 1,
    );
  }
  $subform['subtypes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Regional sub-type settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  $labels = countries_regional_type_labels();
  foreach (countries_regional_types() as $level => $label) {
    $level_values = empty($values['subtypes'][$level]) ? array() : $values['subtypes'][$level];
    $subform['subtypes'][$level]['types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled sub-types for @label', array('@label' => $labels[$level])),
      '#default_value' => empty($level_values['types']) ? array() : $level_values['types'],
      '#options' => $options
    );
    $subform['subtypes'][$level]['required'] = array(
      '#type' => 'checkbox',
      '#title' => t('Required.'),
      '#default_value' => empty($level_values['required']) ? 0 : 1,
    );
  }

  $form['submit']['#submit'][] = 'countries_regions_country_admin_form_submit';
  return $subform;
}

function countries_regions_country_admin_form_submit($form, &$form_state) {
  $regions = array();
  foreach ($form_state['values']['data']['regions'] as $region) {
    if (!empty($region['label']) || !empty($region['type'])) {
      $regions[] = $region;
    }
  }
  $form_state['values']['data']['regions'] = $regions;
  $form_state['values']['data']['max_depth'] = count($regions);
  if ($form_state['values']['data']['rendering_depth']) {
    if ($form_state['values']['data']['rendering_depth'] > $form_state['values']['data']['max_depth']) {
      $form_state['values']['data']['rendering_depth'] = $form_state['values']['data']['max_depth'];
    }
  }
}

/**
 * Callback for hook_countries_configuration_options().
 */
function countries_regions_country_admin_form_title($country) {
  return t('Edit !name regional settings', array('!name' => $country->name));
}


function theme_countries_regions_country_admin_form($variables) {
  $form = $variables['form'];

  $header = array(
    t('Depth'),
    t('Label'),
    t('Type'),
    t('Exposed'),
  );

  $rows = array();
  for ($i = 0; $i < COUNTRIES_REGIONS_MAX_DEPTH; $i++) {
    $row = array();
    $row[] = $i;
    $form[$i]['label']['#title_display'] = 'invisible';
    $form[$i]['label']['#size'] = 15;
    $form[$i]['type']['#title_display'] = 'invisible';
    $form[$i]['exposed']['#title_display'] = 'invisible';
    $row[] = drupal_render($form[$i]['label']);
    $row[] = drupal_render($form[$i]['type']);
    $row[] = drupal_render($form[$i]['exposed']);
    $rows[] = $row;
  }

  $output = t('<p>Each row that has values, either a label or type, is counted to define the maximum levels that can be used for this country and also provides label overrides.</p>');
  $output .= t('<p>The exposed flag is sometimes used to control how a regional path is rendered. This path is a list of regional names from the current region up to optionally include the country and continent.</p>');
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'regions-configuration')));
  $output .= drupal_render_children($form);
  return $output;
}
