<?php

/**
 * @file
 * Contains administrative region callbacks and other rarely called hooks.
 */

/**
 * Callback for countries_regions_menu()
 */
function _countries_regions_menu() {
  $items = array();

  // A placeholder page for the regions bundle.
  $items['admin/config/regional/regions'] = array(
    'title' => 'Regions',
    'description' => 'Regions overview.',
    'page callback' => 'countries_regions_admin_overview',
    'access arguments' => array('administer site configuration'),
    'file' => 'countries_regions.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/regional/regions/overview'] = array(
    'title' => 'Overview',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );
  $items['admin/config/regional/regions/regional-types'] = array(
    'title' => 'Regional types',
    'description' => 'Defines regional sub-types.',
    'page callback' => 'countries_regions_regional_subtypes',
    'access arguments' => array('administer site configuration'),
    'file' => 'countries_regions.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/regional/regions/regional-types/add'] = array(
    'title' => 'Add regional subtype',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('countries_regions_regional_subtype_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_VISIBLE_IN_BREADCRUMB,
    'file' => 'countries_regions.admin.inc',
  );
  $items['admin/config/regional/regions/regional-types/%countries_regions_subtype/edit'] = array(
    'title' => 'Edit regional subtype',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('countries_regions_regional_subtype_form', 5),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_VISIBLE_IN_BREADCRUMB,
    'file' => 'countries_regions.admin.inc',
  );
  $items['admin/config/regional/regions/regional-types/%countries_regions_subtype/delete'] = array(
    'title' => 'Delete regional subtype',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('countries_regions_regional_subtype_delete_form', 5),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_VISIBLE_IN_BREADCRUMB,
    'file' => 'countries_regions.admin.inc',
  );

  // Main regional administrative items.
  $items['admin/config/regional/countries/%country/regions'] = array(
    'title' => 'Regions',
    'title callback' => 'countries_regions_title',
    'title arguments' => array(4),
    'description' => 'List, edit, or add regions.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('countries_regions_country_admin_overview', 4),
    'access arguments' => array('administer site configuration'),
    'file' => 'countries_regions.admin.inc',
    'weight' => -5,
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/regional/countries/%country/regions/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );
  $items['admin/config/regional/countries/%country/regions/add'] = array(
    'title' => 'Add region',
    'page callback' => 'countries_regions_admin_page',
    'page arguments' => array(4),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_ACTION,
    'weight' => 1,
    'file' => 'countries_regions.admin.inc',
  );
  $items['admin/config/regional/countries/%country/regions/%countries_regions'] = array(
    'type' => MENU_VISIBLE_IN_BREADCRUMB,
    'page callback' => 'countries_regions_admin_page',
    'page arguments' => array(4, 6),
    'access arguments' => array('administer site configuration'),
    'file' => 'countries_regions.admin.inc',
  );
  $items['admin/config/regional/countries/%country/regions/%countries_regions/edit'] = array(
    'title' => 'Edit',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/config/regional/countries/%country/regions/%countries_regions/delete'] = array(
    'title' => 'Delete region',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('countries_regions_admin_delete', 4, 6),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_VISIBLE_IN_BREADCRUMB,
    'file' => 'countries_regions.admin.inc',
  );

  return $items;
}

/**
 * Menu callback; Placeholder for the field settings of the region entity.
 */
function countries_regions_admin_overview() {
  $build['countries_regions_pager'] = array(
    '#markup' => t('<p>Regions can be extended like other core bundles by adding various fields using the %fields and %display tabs. However, there is no inbuilt method of using these.', array('%fields' => t('Manage fields'), '%display' => t('Manage display')))
               . t('<p>Regions are administrated under the countries that they are assigned.</p>'),
  );

  return $build;
}

/**
 * TODO: This needs some more thought.
 *
 * The tree should show the recommended rendering depth, but it should allow
 * dragging to the full depth allowed at any one stage.
 */
function countries_regions_country_admin_overview($form, &$form_state, $country, $region_rid = NULL) {
  // Generate a partial regional tree for a country.
  $settings = countries_configuration($country, 'countries_regions');

  // The settings for theming the listing form.
  $parental_depth = empty($region_rid) ? 0 : count(countries_regions_parents($region_rid));

  // We set the tree to limit the dragable depth, and set the query to set the
  // depth of the tree rendered.
  $form['#settings'] = array(
    'allowed_depth' => $settings['max_depth'] - $parental_depth,
  );
  $rendering_depth = empty($settings['rendering_depth']) ? $settings['max_depth'] : $settings['rendering_depth'];

  // The settings for querying the subregion tree.
  $query_settings = array(
    'max_depth' => max(array($rendering_depth, 1)),
    'include_child_nodes' => 1,
  );
  $parent_region = empty($region_rid) ? NULL : countries_regions_load($region_rid);

  $query_settings['max_depth'] = 1;
  $tree = countries_regions_tree($country->iso2, $parent_region, $query_settings);
  $form = array_merge($form, _countries_regions_country_admin_overview_form($country, $tree, $parent_region));
  $form['#country'] =  $country;
  $form['#region'] =  $parent_region;

  if (element_children($form)) {
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save regions'),
    );
    $form['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array('countries_regions_country_admin_overview_reset_submit'),
    );
  }
  else {
    $options = array('query' => array());
    if ($parent_region) {
      $options['query'] = array(
        'prid' => $parent_region->rid,
      );
    }
    $options['query'] += drupal_get_destination();
    $name = $parent_region ? $parent_region->name : $country->name;
    $form['#empty_text'] = t('There are no regions yet. <a href="@link">Add region</a> for %name.',
      array('@link' => url('admin/config/regional/countries/' . $country->iso2 . '/regions/add', $options), '%name' => $name));
  }
  return $form;
}

/**
 * Submit handler for the country region overview form.
 *
 * This function takes great care in saving parent regions first, then regions
 * underneath them. Saving regions in the incorrect order can break the country
 * region tree.
 *
 * @see countries_regions_country_admin_overview()
 */
function countries_regions_country_admin_overview_submit($form, &$form_state) {
  $order = array_flip(array_keys($form_state['input']));
  $form = array_merge($order, $form);

  $updated_regions = array();
  $fields = array('weight', 'prid');
  foreach (element_children($form) as $rid) {
    if (isset($form[$rid]['#region'])) {
      $element = $form[$rid];
      // Update any fields that have changed in this menu item.
      foreach ($fields as $field) {
        if ($element[$field]['#value'] != $element[$field]['#default_value']) {
          $element['#region']->{$field} = $element[$field]['#value'];
          $updated_regions[$rid] = $element['#region'];
        }
      }
    }
  }

  // Save all our changed items to the database.
  foreach ($updated_regions as $region) {
    countries_regions_region_save($region);
  }

  // Traverse the tree to discover any type updates.
  $updates = array();
  $settings = array(
    'max_depth' => COUNTRIES_REGIONS_MAX_DEPTH,
  );
  $children = countries_regions_tree($form['#country'], NULL, $settings);
  foreach ($children as $subregion) {
    $updates += _countries_regions_type_update_required($subregion, NULL, NULL, TRUE);
  }

  // TODO: This should be a bulk SQL update. I just need to remember how...
  if (!empty($updates)) {
    foreach ($updates as $rid => $type) {
      db_update('countries_region')
        ->fields(array('type' => $type))
        ->condition('rid', $rid)
        ->execute();
    }
    module_invoke_all('region_bulk_type_update', $updates);
  }

  drupal_set_message(t('Your configuration has been saved.'));
}

/**
 * Submit handler for the country region overview form.
 *
 * This function simple sets the weights to zero, reseting the tree to an
 * alphabetical order.
 *
 * Since only the weight is touched, no other hooks are called.
 */
function countries_regions_country_admin_overview_reset_submit($form, &$form_state) {
  // Update the weights to zero.
  db_update('countries_region')
    ->fields(array(
      'weight' => 0,
    ))
    ->condition('iso2', $form['#country']->iso2)
    ->execute();
}

/**
 * Helper function to get the maximum depth for a country.
 */
function countries_regions_maximum_depth($country) {
  $settings = countries_configuration($country, 'countries_regions');
  return $settings['max_depth'];
}

/**
 * Recursive helper function for countries_regions_country_admin_overview().
 *
 * @param $tree
 *   The menu_tree retrieved by menu_tree_data.
 */
function _countries_regions_country_admin_overview_form($country, $regions, $parent_region) {
  $form = &drupal_static(__FUNCTION__, array('#tree' => TRUE));

  $destination = array('destination' => 'admin/config/regional/countries/' . $country->iso2 . '/regions');

  foreach ($regions as $region) {
    $title = '';
    $rid = 'rid:' . $region->rid;
    $form[$rid]['#region'] = $region;
    $form[$rid]['title']['#markup'] = check_plain($region->name); //l($region->name, $item['href'], $item['localized_options']);
    $form[$rid]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 50,
      '#default_value' => $region->weight,
      '#title_display' => 'invisible',
      '#title' => t('Weight for @title', array('@title' => $region->name)),
    );
    $form[$rid]['rid'] = array(
      '#type' => 'hidden',
      '#value' => $region->rid,
    );
    $form[$rid]['prid'] = array(
      '#type' => 'hidden',
      '#default_value' => $region->prid,
    );

    // Get country options and region statistics
    $max_depth = countries_regions_maximum_depth($country);
    $parents = countries_regions_parents($region->prid);
    $count = countries_regions_count($country, $region);

    // Build a list of operations for the subregions.
    $subregion_operations = array();
    $subregion_operations['count'] = array(
      '#markup' => format_plural($count, t('1 region'), t('@count regions')),
    );
    $too_deep = count($parents) + 1 >= $max_depth;
    if (!$too_deep) {
      $subregion_operations['add'] = array(
        '#type' => 'link',
        '#title' => t('add'),
        '#href' => 'admin/config/regional/countries/' . $region->iso2 . '/regions/add',
        '#options' => array(
          'query' => array('prid' => $region->rid) + drupal_get_destination(),
        ),
      );
    }
    else {
      $subregion_operations['add'] = array(
        '#markup' => t('<del title="Region is too deep to add more subregions">add</del>'),
      );
    }
    if ($count || !$too_deep) {
      $subregion_operations['subregions'] = array(
        '#type' => 'link',
        '#title' => t('list'),
        '#href' => 'admin/config/regional/countries/' . $region->iso2 . '/regions/list/' . $region->rid,
      );
    }
    else {
      $subregion_operations['subregions'] = array(
        '#markup' => t('<del title="Region is too deep to list subregions">list</del>'),
      );
    }
    $form[$rid]['subregions'] = $subregion_operations;

    // Build a list of operations.
    $operations = array();
    $operations['edit'] = array('#type' => 'link', '#title' => t('edit'), '#href' => 'admin/config/regional/countries/' . $region->iso2 . '/regions/' . $region->rid);
    $operations['delete'] = array('#type' => 'link', '#title' => t('delete'), '#href' => 'admin/config/regional/countries/' . $region->iso2 . '/regions/' . $region->rid . '/delete');
    $form[$rid]['operations'] = $operations;

    if (!empty($region->subregions)) {
      _countries_regions_country_admin_overview_form($country, $region->subregions, $parent_region);
    }
  }
  return $form;
}

function theme_countries_regions_country_admin_overview($variables) {
  $form = $variables['form'];

  $country = $form['#country'];
  $region = empty($form['#region']) ? FALSE : $form['#region'];

  $allowed_depth = $form['#settings']['allowed_depth'] - 1;

  // Passing 0 to drupal_add_tabledrag() still enables dragging!
  // Without this, we can not use dnd to fix the depth :(
  // See http://drupal.org/node/1310642
//  if ($allowed_depth > 0) {
    drupal_add_tabledrag('regions-overview', 'match', 'parent', 'regions-prid', 'regions-prid', 'regions-rid', TRUE, $allowed_depth);
//  }
  drupal_add_tabledrag('regions-overview', 'order', 'sibling', 'regions-weight');

  $parent_link = FALSE;
  if ($region) {
    if ($region->prid == 0) {
      $parent_link = ' ' . l(t('View top level regions'), 'admin/config/regional/countries/' . $region->iso2 . '/regions');
    }
    else {
      $parent = countries_regions_load($region->prid);
      $parent_link = ' ' . l(t('Move up one level to !region regions', array('!region' => $parent->name)),
          'admin/config/regional/countries/' . $region->iso2 . '/regions/list/' . $region->prid);
    }
  }
  $header = array(
    $region
      ? t('Regions in %region, %country.', array('%region' => $region->name, '%country' => $country->name)) . $parent_link
      : t('Top level regions in %country', array('%country' => $country->name)),
    t('Type'),
    t('Weight'),
    array('data' => t('Subregions'), 'colspan' => '3'),
    array('data' => t('Operations'), 'colspan' => '2'),
  );

  $rows = array();

  $labels = countries_regional_types($country) + countries_regional_type_labels();
  if (isset($labels[t('Additional types')])) {
    $at = $labels[t('Additional types')] ;
    unset($labels[t('Additional types')]);
    $labels += $at;
  }

  foreach (element_children($form) as $rid) {
    if (isset($form[$rid]['#region'])) {
      $element = &$form[$rid];
      // Build a list of subregion operations.
      $subregion_operations = array();
      foreach (element_children($element['subregions']) as $op) {
        $subregion_operations[] = array('data' => drupal_render($element['subregions'][$op]), 'class' => array('regions-subregions-operations'));
      }

      // Build a list of operations.
      $operations = array();
      foreach (element_children($element['operations']) as $op) {
        $operations[] = array('data' => drupal_render($element['operations'][$op]), 'class' => array('regions-operations'));
      }

      // Add special classes to be used for tabledrag.js.
      $element['prid']['#attributes']['class'] = array('regions-prid');
      $element['rid']['#attributes']['class'] = array('regions-rid');
      $element['weight']['#attributes']['class'] = array('regions-weight');

      // Change the parent field to a hidden. This allows any value but hides the field.
      $element['prid']['#type'] = 'hidden';

      $row = array();
      $synonym_hint = '';
      if (!empty($form[$rid]['#region']->synonym)) {
        $synonym_ref = countries_regions_load($form[$rid]['#region']->synonym);
        $link = l('1', 'admin/config/regional/countries/' . $form[$rid]['#region']->iso2 . '/regions/' . $form[$rid]['#region']->synonym,
            array('target' => '_blank', 'attributes' => array('title' => t('This region is based on the region %region.', array('%region' => $synonym_ref->name)))));
        $synonym_hint = '<sup>' . $link . '</sup>';
      }
      $addr_exclude_hint = empty($form[$rid]['#region']->addr_exclude) ? '' : '<sup>2</sup>';
      $note = '';
      if (!empty($form[$rid]['#region']->note)) {
        $note = '<br /><em>' . check_plain($form[$rid]['#region']->note) . '</em>';
      }
      $title = '<span>' . drupal_render($element['title']) . $synonym_hint . $addr_exclude_hint . $note . '</span>';

      $row[] = theme('indentation', array('size' => $element['#region']->depth)) . $title;

      $type_selector_hint = empty($form[$rid]['#region']->type_selector) ? '<sup>3</sup>' : '';
      if (empty($element['#region']->type)) {
        $type = t('Unknown');
      }
      else{
        $type_key = $element['#region']->type . '-' . $element['#region']->subtype;
        if (isset($labels[$type_key])) {
          $type = $labels[$type_key];
        }
        else {
          $type = $labels[$element['#region']->type];
        }
      }
      $row[] = $type . $type_selector_hint;
      $row[] = drupal_render($element['weight']) . drupal_render($element['prid']) . drupal_render($element['rid']);
      $row = array_merge($row, $subregion_operations);
      $row = array_merge($row, $operations);

      $row = array_merge(array('data' => $row), $element['#attributes']);
      $row['class'][] = 'draggable';
      $rows[] = $row;
    }
  }
  $output = '';
  if (empty($rows)) {
    $rows[] = array(array('data' => $form['#empty_text'], 'colspan' => '8'));
  }
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'regions-overview')));
  $output .= t('<ol><li>This region is a secondary reference to another region.</li><li>This region may be excluded from regional paths and addresses.</li><li>This region has no defined regional type, so the type may change if it is moved in the regional tree.</li></ol>');
  $output .= drupal_render_children($form);
  return $output;
}



/**
 * Menu callback; Display a region form.
 */
function countries_regions_admin_page($country, $region = NULL) {
  if (!isset($region->rid)) {
    drupal_set_title(t('New region for @country', array('@country' => $country->name)), PASS_THROUGH);
    $region = countries_regions_default_object($country);
  }
  else {
    $base_region = countries_regions_base_synonym($region);
    if ($base_region->rid != $region->rid) {
      drupal_goto('admin/config/regional/countries/' . $base_region->iso2 . '/regions/' . $base_region->rid);
    }
  }
  return drupal_get_form('countries_regions_admin_form', $country, $region);
}

/**
 * Generate a region form.
 *
 * @ingroup forms
 * @see countries_regions_admin_form_validate()
 * @see countries_regions_admin_form_submit()
 */
function countries_regions_admin_form($form, &$form_state, $country, $region) {
  // Menu callbacks do not have page titles.
  if ($region->rid) {
    drupal_set_title(t('Edit region @region, @country', array('@country' => $country->name, '@region' => $region->name)), PASS_THROUGH);
  }
  // We can define the parent region on new regions only
  elseif (!empty($_REQUEST['prid']) && is_numeric($_REQUEST['prid'])) {
    $invalid_prid = TRUE;
    if ($parent = countries_regions_load($_REQUEST['prid'])) {
      if ($parent->iso2 == $region->iso2) {
        $region->prid = $parent->rid;
        drupal_set_title(t('New region for @region, @country', array('@country' => $country->name, '@region' => $parent->name)), PASS_THROUGH);
        $invalid_prid = FALSE;
      }
    }
    if ($invalid_prid) {
      drupal_set_message(t('Invalid parent region specified. Ignoring!'), 'error');
    }
  }

  $parents = countries_regions_parents($region->prid);
  $max_depth = countries_regions_maximum_depth($country);
  if (count($parents) >= $max_depth) {
    drupal_set_message(t('Region depth is too deep for this country!'), 'error');
    drupal_goto('admin/config/regional/countries/' . $country->iso2 . '/regions');
  }

  $form = array();
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $region->rid,
  );
  $form['prid'] = array(
    '#type' => 'value',
    '#value' => $region->prid,
  );
  $regional_parent_options = array('' => t('[ empty/remove ]'), '0' => t('No parent'));

  $settings['max_depth'] = $max_depth;
  $settings['include_child_nodes'] =  1;
  $settings['filters']['enabled'] = COUNTRIES_ENABLED;
  $settings['filters']['countries'] = array($region->iso2);
  if (!empty($region->rid)) {
    $settings['types_above'] = $region->type + 1;
    $settings['exclude'] = array($region->rid);
  }
  if (!empty($region->prid)) {
    $parent = countries_regions_load($region->prid);
    $settings['types_above'] = $parent->type + 2;
  }

  $parent = array_shift($parents);
  $tree = countries_regions_tree(NULL, $parent, $settings + array('max_depth' => 2));
  foreach ($tree as $iso2 => $info) {
    $regional_parent_options += countries_regions_options($info->subregions, '');
  }
  $form['iso2'] = array(
    '#type' => 'value',
    '#value' => $region->iso2,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $region->name,
    '#description' => t('Specify a unique name for this region.'),
    '#required' => TRUE,
    '#maxlength' => 255,
  );

  $form['note'] = array(
    '#type' => 'textfield',
    '#title' => t('Administation note'),
    '#default_value' => $region->note,
    '#description' => t('A note to assist others administrators.'),
    '#maxlength' => 255,
  );

  $form['code'] = array(
    '#type' => 'textfield',
    '#title' => t('Region code'),
    '#default_value' => $region->code,
    '#description' => t('Specify a unique code for this region.'),
    '#maxlength' => 28,
  );

  $regional_types = array('' => t('-- autodetect --')) + countries_regional_types($country);

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Regional type'),
    '#default_value' => $region->type . '-' . $region->subtype,
    '#options' => $regional_types,
  );
  // This is used to determine if we need to recalculate child regions for a
  // change in the depth.
  $form['old_type'] = array(
    '#type' => 'value',
    '#value' => $region->type,
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 95,
  );
  $form['advanced']['type_selector'] = array(
    '#type' => 'checkbox',
    '#title' => t('Lock regional type'),
    '#default_value' => $region->type_selector,
    '#description' => t('Without this lock, the regional type is calculated from the regions depth and the countries defined regional types. This is normally locked, but you can uncheck this so that the regional type and its child regions will dynamically adjust their type as their location in the regional tree changes. If a regional types subtype is entered, this will automatically lock the regional type on save.'),
  );
  $form['advanced']['addr_exclude'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude from address or regional paths'),
    '#default_value' => $region->addr_exclude,
    '#description' => t('This is simply an indicator flag to suggest that this region is not important in address or regional paths.'),
  );

  $form['synonyms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Multi-parent relationships'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 95,
    '#tree' => TRUE,
    '#description' => t('These options allow you to create additional regional parents so that a single region can be a sub-region of multiple parents. This is often needed when sub-regions overlap 2 or more parental regions.'),
    '#theme' => 'countries_regions_regional_synonyms_form',
  );
  $synonyms = empty($region->rid) ? array() : countries_regions_synonyms($region);

  $synonyms['new-0'] = countries_regions_default_object($region->iso2);
  $synonyms['new-1'] = countries_regions_default_object($region->iso2);
  $synonyms['new-3'] = countries_regions_default_object($region->iso2);
  foreach ($synonyms as $rid => $synonym) {
    $form['synonyms'][$rid] = array();
    $form['synonyms'][$rid]['prid'] = array(
      '#type' => 'select',
      '#title' => t('Parent'),
      '#default_value' => $synonym->rid ? $synonym->prid : '',
      '#options' => $regional_parent_options,
    );
    $form['synonyms'][$rid]['rid'] = array(
      '#type' => 'value',
      '#value' => $synonym->rid,
    );
    $form['synonyms'][$rid]['note'] = array(
      '#type' => 'textfield',
      '#title' => t('Note'),
      '#default_value' => $synonym->note,
    );
    $form['synonyms'][$rid]['remove_options'] = array(
      '#type' => 'radios',
      '#title' => t('Remove'),
      '#default_value' => 0,
      '#options' => array(
        0 => t('No delete'),
        1 => t('Decouple'),
        2 => t('Delete, reparent subregions'),
        3 => t('Delete, including subregions'),
      ),
      '#disabled' => !$synonym->rid,
    );
  }

  $form['#country'] = $country;
  if (!empty($region->rid)) {
    $form['#region'] = $region;
  }
  $form['#config'] = countries_configuration($country, 'countries_regions');

  $region = (object) $region;
  field_attach_form('country_region', $region, $form, $form_state);

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'), '#weight' => 100);

  return $form;
}

/**
 * Validate region form submissions.
 */
function countries_regions_admin_form_validate($form, &$form_state) {
  $form_state['values']['name'] = trim($form_state['values']['name']);
  $form_state['values']['code'] = trim($form_state['values']['code']);

  $iso2 = $form['#country']->iso2;
  $prid = $form_state['values']['prid'];

  // Validate the unique name restriction.
  if (drupal_strlen($form_state['values']['name'])) {
    if (empty($form['#region'])) {
      if ((bool) db_query_range("SELECT 1 FROM {countries_region} WHERE iso2 = :iso2 AND prid = :prid AND LOWER(name) = LOWER(:name)", 0, 1, array(':iso2' => $iso2, 'prid' => $prid, ':name' => $form_state['values']['name']))->fetchField()) {
        form_set_error('name', t('The name %value is already taken.', array('%value' => $form_state['values']['name'])));
      }
    }
    else {
      if ((bool) db_query_range("SELECT 1 FROM {countries_region} WHERE iso2 = :iso2 AND prid = :prid AND rid <> :rid AND LOWER(name) = LOWER(:name)", 0, 1, array(':iso2' => $iso2, 'prid' => $prid, ':rid' => $form['#region']->rid, ":name" => $form_state['values']['name']))->fetchField()) {
        form_set_error('name', t('The value %value is already taken.', array('%value' => $form_state['values']['name'])));
      }
    }
  }

  // Discover how and if we need to autocalculate the region type. The empty
  // type option is auto-detect. However, if a subtype is used, other than the
  // default value will result in this locking.
  list($type,$subtype) = explode('-', $form_state['values']['type'] . '-');
  $form_state['values']['type'] = $type;
  if (!empty($subtype)) {
    $form_state['values']['type_selector'] = 1;
    $form_state['values']['subtype'] = $subtype;
  }
  else {
    $form_state['values']['subtype'] = 0;
  }

  // Automatically detect the regions type if the lock is not set or the type is
  // not set.
  if (!$form_state['values']['type_selector'] || empty($type)) {
    $settings = countries_configuration($form['#country'], 'countries_regions');
    $parent = empty($prid) ? NULL : countries_regions_load($prid);
    $form_state['values']['type'] = _countries_regions_regional_type_from_parent($parent, $settings['regions']);
  }
}

/**
 * Process region form submissions.
 */
function countries_regions_admin_form_submit($form, &$form_state) {
  $region = (object) $form_state['values'];
  entity_form_submit_build_entity('country_region', $region, $form, $form_state);

  if (countries_regions_region_save($region)) {
    $message = t('Added region %region to %country.', array('%region' => $region->name, '%country' => $form['#country']->name));
  }
  else {
    $message = t('The region %region in %country has been updated.', array('%region' => $region->name, '%country' => $form['#country']->name));
  }
  drupal_set_message($message);
  $form_state['redirect'] = 'admin/config/regional/countries/' . $region->iso2 . '/regions';
}

function theme_countries_regions_regional_synonyms_form($variables) {
  $form = &$variables['form'];
  $header = array();
  $rows = array();
  $additional = '';
  foreach (element_children($form) as $rid) {
    $children = element_children($form[$rid]);
    if (empty($header)) {
      foreach ($children as $child) {
        if ($form[$rid][$child]['#type'] == 'value') {
          continue;
        }
        $title = $form[$rid][$child]['#title'];
        if (isset($form[$rid][$child]['#description'])) {
          $header[] = '<span title="' . check_plain($form[$rid][$child]['#description']) . '">' . check_plain($title) . '</span>';
        }
        else {
          $header[] = check_plain($title);
        }
      }
    }
    $row = array();
    foreach ($children as $child) {
      if ($form[$rid][$child]['#type'] == 'value') {
        $additional .= drupal_render($form[$rid][$child]);
      }
      else {
        $form[$rid][$child]['#title_display'] = 'invisible';
        $form[$rid][$child]['#description'] = NULL;
        $row[] = drupal_render($form[$rid][$child]);
      }
    }

    $rows[] = $row;
  }
  return theme('table', array('header' => $header, 'rows' => $rows))
    . $additional
    . t('<p><strong>Notes</strong><ul><li>If you do not specify a parent region, new records will not be added.</li></ul></p>');
}

/**
 * Menu callback; confirm deletion of a region.
 *
 * @ingroup forms
 * @see countries_regions_admin_delete_submit()
 */
function countries_regions_admin_delete($form, &$form_state, $country, $region) {
  $form['#country'] = $country;
  $form['#region'] = $region;

  $form['remove_subregions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove subregions'),
    '#default_value' => 1,
  );
  $form['remove_synonyms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove related regions.'),
    '#default_value' => 0,
  );
  return confirm_form($form,
    t('Are you sure you want to delete the region %region from %country?', array('%region' => $region->name, '%country' => $country->name)),
    'admin/config/regional/countries/' . $region->iso2 . '/regions',
    t('References that use this region will become invalid. This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Process regions delete form submission.
 */
function countries_regions_admin_delete_submit($form, &$form_state) {
  $country = $form['#country'];
  $region = $form['#region'];

  countries_regions_region_delete($region, $form_state['values']['remove_subregions'], $form_state['values']['remove_synonyms']);

  drupal_set_message(t('Deleted region %region from %country.', array('%region' => $region->name, '%country' => $country->name)));
  $form_state['redirect'] = 'admin/config/regional/countries/' . $region->iso2 . '/regions';
}

/**
 * Regional subtype listing.
 */
function countries_regions_regional_subtypes() {
  $query = db_select('countries_regional_subtype', 'st');
  $query->fields('st');
  $query->orderBy('st.name', 'ASC');

  $labels = countries_regional_type_labels();
  $header = array(
    t('Name'),
    t('Code'),
    t('Operations'),
  );
  $rows = array();
  foreach ($query->execute() as $type) {
    $row = array();
    $row[] = check_plain($type->name);
    $row[] = check_plain($type->code);
    $operations = array();
    $operations[] = l(t('edit'), 'admin/config/regional/regions/regional-types/' . $type->stid . '/edit');
    if (!$type->locked) {
      $operations[] = l(t('delete'), 'admin/config/regional/regions/regional-types/' . $type->stid . '/delete');
    }
    $row[] = implode('&nbsp;&nbsp;&nbsp;', $operations);
    $rows[] = $row;
  }
  $page['build'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  return $page;
}

function countries_regions_regional_subtype_form($form, &$form_state, $subtype = NULL) {
  if (!$subtype) {
    $subtype = (object) array(
      'stid' => 0,
      'name' => '',
      'code' => '',
      'locked' => 0,
      'behaviour' => 0,
    );
  }
  $form['stid'] = array(
    '#type' => 'value',
    '#value' => $subtype->stid,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $subtype->name,
    '#maxlength' => 127,
    '#required' => TRUE,
  );
  $form['code'] = array(
    '#type' => 'machine_name',
    '#title' => t('Code'),
    '#default_value' => $subtype->code,
    '#maxlength' => 63,
    '#description' => NULL,
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'country_regions_regional_subtype_exists',
      'source' => array('name'),
      'replace_pattern' => '[^a-z-]+',
      'replace' => '-',
    ),
    '#disabled' => $subtype->locked,
  );
  $form['plural'] = array(
    '#type' => 'textfield',
    '#title' => t('Plural'),
    '#default_value' => $subtype->plural,
    '#maxlength' => 127,
    '#required' => TRUE,
  );
  $form['behaviour'] = array(
    '#type' => 'radios',
    '#title' => t('Behaviour'),
    '#default_value' => $subtype->behaviour,
    '#description' => t('Defines the behaviour of this subtype in relation to the regional tree. Fixed regions must reside within the strict boundaries of the regional tree. Floating regions have no fixed position and can reside at any level in the tree. Floating regions need to be enabled globally or in a specific country before they are selected as a regions type.'),
    '#required' => TRUE,
    '#options' => array(
      0 => t('Fixed'),
      1 => t('Floating'),
      2 => t('Both'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validate using the region type and the machine code.
 */
function country_regions_regional_subtype_exists($value, $element, $form_state) {
  return db_query_range('SELECT 1 FROM {countries_regional_subtype} WHERE code = :code',
       0, 1, array(':code' => $value))->fetchField();
}

function countries_regions_regional_subtype_form_submit($form, &$form_state) {
  $record = (object) $form_state['values'];
  if ($record->stid) {
    drupal_write_record('countries_regional_subtype', $record, array('stid'));
  }
  else {
    $record->stid = NULL;
    drupal_write_record('countries_regional_subtype', $record);
  }
  $form_state['redirect'] = 'admin/config/regional/regions/regional-types';
}

/**
 * Menu callback; confirm deletion of a regional subtype.
 */
function countries_regions_regional_subtype_delete_form($form, &$form_state, $subtype) {
  if ($subtype->locked) {
    drupal_set_message(t('You can not delete the default regional subtypes.'), 'error');
    drupal_goto('admin/config/regional/regions/regional-types');
  }

  $form['#subtype'] = $subtype;
  return confirm_form($form,
    t('Are you sure you want to delete the regional sub-region type %subtype?', array('%subtype' => $subtype->name)),
    'admin/config/regional/regions/regional-types',
    t('References that use this type will become invalid. This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Process regions delete form submission.
 */
function countries_regions_regional_subtype_delete_form_submit($form, &$form_state) {
  $subtype = $form['#subtype'];
  db_delete('countries_regional_subtype')->condition('stid', $subtype->stid)->execute();
  drupal_set_message(t('Deleted regional sub-type %subtype.', array('%subtype' => $subtype->name)));
  $form_state['redirect'] = 'admin/config/regional/regions/regional-types';
}
