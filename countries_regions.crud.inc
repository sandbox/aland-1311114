<?php

/**
 * @file
 * Contains all of the CRUD functions for regions.
 */

/**
 * Loads multiple regions.
 */
function countries_regions_load_multiple($rids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('country_region', $rids, $conditions, $reset);
}

/**
 * Loads a region.
 */
function countries_regions_load($rid, $reset = FALSE) {
  if (empty($rid)) {
    return NULL;
  }
  $regions = countries_regions_load_multiple(array($rid), array(), $reset);
  return reset($regions);
}

/**
 * Generates a default regions object.
 */
function countries_regions_default_object($country, $info = array()) {
  return (object)($info + array(
    'rid' => 0,
    'prid' => 0,
    'synonym' => 0,
    'iso2' => is_object($country) ? $country->iso2 : $country,
    'name' => '',
    'note' => '',
    'population' => 0,
    'code' => '',
    'type' => 0,
    'type_selector' => 1,
    'addr_exclude' => 0,
    'subtype' => 0,
  ));
}

/**
 * Saves a region.
 *
 * @return
 *   Returns TRUE if it is an insert.
 */
function countries_regions_region_save(&$region, $flush = TRUE) {
  field_attach_presave('country_region', $region);

  if (!empty($region->rid)) {
    drupal_write_record('countries_region', $region, array('rid'));
    field_attach_update('country_region', $region);

    countries_regions_region_update_synonyms($region);

    // Synonym updates do not flush the cache.
    if ($flush) {
      field_cache_clear();
    }
    module_invoke_all('countries_region_update', $region);

    // Now recursively hit children regions to update the regional type.
    // This collects up all updates required, runs a single mass update SQL
    // statement before calling a special hook to allow other modules react
    // to the changes.
    if (!empty($region->old_type) && $region->old_type != $region->type) {
      $updates = array();
      $settings = array(
        'max_depth' => COUNTRIES_REGIONS_MAX_DEPTH,
      );
      $children = countries_regions_tree($region->iso2, $region, $settings);
      foreach ($children as $subregion) {
        $updates += _countries_regions_type_update_required($subregion, $region);
      }

      // TODO: This should be a bulk update.
      if (!empty($updates)) {
        foreach ($updates as $rid => $type) {
          db_update('countries_region')
            ->fields(array('type' => $type))
            ->condition('rid', $rid)
            ->execute();
        }
        module_invoke_all('countries_region_bulk_type_update', $updates);
      }
    }
    return 0;
  }
  else {
    drupal_write_record('countries_region', $region);
    field_attach_insert('country_region', $region);

    countries_regions_region_update_synonyms($region);

    module_invoke_all('countries_region_insert', $region);

    return 1;
  }
}


/**
 * This creates a strict heirachical path from the selected region to the base
 * parental region.
 *
 * @param $rid
 *   The region rid.
 *
 * @return $regions
 *   An array of regions ordered from the child to parent regions.
 */
function countries_regions_parents($rid) {
  $regions = array();
  while ($rid) {
    if ($region = countries_regions_load($rid)) {
      $regions[$region->rid] = $region;
      $rid = $region->prid;

      // Recursion prevention. We shouldn't need this, but it protects against
      // invalid imports or errors.
      if (array_key_exists($rid, $regions)) {
        break;
      }
    }
    else {
      break;
    }
  }
  return $regions;
}

/**
 * Loads the subregions for a given region.
 */
function countries_regions_children($region) {
  $rids = db_select('countries_region', 'r')
    ->fields('r', array('rid'))
    ->condition('r.prid', $region->rid)
    ->execute()
    ->fetchCol();
  return countries_regions_load_multiple($rids);
}

/**
 * Simple wrapper to get the region where the synonyms were created.
 */
function countries_regions_base_synonym($region) {
  if (!empty($region->synonym)) {
    return countries_regions_load($region->synonym);
  }
  return $region;
}

/**
 * Helper function to load related regions.
 *
 * @param object $region
 * @param bool $use_base_synonym
 *   By default, this function will return any synonyms for a regional
 *   relationship, but this flag can be used to prevent checks of the main base
 *   region, (unless the region passed is the main base region).
 */
function countries_regions_synonyms($region, $use_base_synonym = TRUE) {
  // New regions can not have any related regions.
  if (empty($region->rid)) {
    return array();
  }

  $synonyms = &drupal_static(__FUNCTION__, FALSE);
  if ($use_base_synonym) {
    $region = countries_regions_base_synonym($region);
  }
  $rid = $region->rid;
  if (!isset($synonyms[$region->rid])) {
    $synonyms[$region->rid] = array();

    $rids = db_select('countries_region', 'r')
      ->fields('r', array('rid'))
      ->condition('r.synonym', $region->rid)
      ->execute()
      ->fetchCol();
    if ($regions = countries_regions_load_multiple($rids)) {
      $synonyms[$region->rid] = $regions;
    }
  }
  return $synonyms[$region->rid];
}

function countries_regions_region_update_synonyms($region) {
  if (!empty($region->synonyms)) {

    // We clone the region and update set parameters.
    foreach ($region->synonyms as $values) {
      // The parent is either an empty string (ie remove) or a integer 0+
      if (empty($values['prid']) && strlen($values['prid']) === 0) {
        $values['remove_options'] = '2';
      }

      // We can not decouple or delete new regions
      if (empty($values['rid']) && $values['remove_options'] > 0) {
        continue;
      }

      if ($values['remove_options'] == '2') {
        // Delete, reparent subregions
        $synonym = countries_regions_load($values['rid']);
        if ($children = countries_regions_children($synonym)) {
          foreach ($children as $child) {
            $child->prid = $region->rid;
            countries_regions_region_save($child, FALSE);
          }
        }
        countries_regions_region_delete($synonym, FALSE, FALSE, FALSE);
        continue;
      }
      elseif ($values['remove_options'] == '3') {
        // Delete, including subregions
        countries_regions_region_delete(countries_regions_load($values['rid']), TRUE, FALSE, FALSE);
        continue;
      }

      // Copy over
      $synonym = clone $region;
      $synonym->synonym = $values['remove_options'] == '1' ? 0 : $region->rid;
      unset($synonym->rid);
      unset($synonym->synonyms);
      foreach ($values as $key => $value) {
        $synonym->$key = $value;
      }
      $synonym->rid = empty($values['rid']) ? NULL : $values['rid'];
      countries_regions_region_save($synonym, FALSE);
      if ($values['remove_options'] == '1') {
        drupal_set_message(t('The region !name has been decoupled from the original region.',
          array('!name' => l($synonym->name, 'http://site9/admin/config/regional/countries/' . $synonym->iso2 . '/regions/' . $synonym->rid))));
      }
    }
  }
}

/**
 * Deletes a region and (optional) sub-regions.
 *
 * If the subregions are not deleted, the subregions are reparented using the
 * following rules:
 *   - if a synonym, reparent to the main region
 *   - if it has a parent, reparent to the regions parent
 *   - reparent subregions to 0
 *
 * If the region is the main synonym, the default behaviour is to delete all of
 * the other synonyms. However, this can be overriden.
 *
 * An overview of what happens with each combination of options.
 *  - a
 *    - b1
 *    - b1s1
 *      - b1c1
 *      - b1c2
 *      - b1c2s1
 *      - b1c2s2
 *    - b1s2
 *    - b2
 *      - b2c1
 *      - b2c2
 *      - b2c2s1
 *      - b2c2s2
 *    - b3
 *
 * a) Delete subregions and synonyms
 *  - a                 14 Delete
 *    - b1              7  Delete
 *    - b1s1            5  Delete
 *      - b1c1          1  Delete
 *      - b1c2          4  Delete
 *      - b1c2s1        2  Delete
 *      - b1c2s2        3  Delete
 *    - b1s2            6  Delete
 *    - b2              12 Delete
 *      - b2c1          8  Delete
 *      - b2c2          11 Delete
 *      - b2c2s1        9  Delete
 *      - b2c2s2        10 Delete
 *    - b3              13 Delete
 *
 * b) Delete subregions and reparent synonyms
 *  - a                 14 Delete
 *    - b1              7  Delete
 *    - b2              12 Delete
 *      - b2c1          8  Delete
 *      - b2c2          11 Delete
 *    - b3              13 Delete
 * Moved into alternative tree
 *    - b1s1            5  Main synonym, same parent
 *      - b1c1
 *      - b1c2
 *    - b1s2            6  New synonym of b1s1
 *      - b1c2s1
 *      - b1c2s2
 * Moved into alternative tree
 *      - b2c2s1        9  Main synonym, same parent
 *      - b2c2s2        10 New synonym of b2c2s1
 *
 * c) Delete synonyms and reparent subregions
 *  - a                 14 Delete
 *    - b1              7  Reparent a parent
 *    - b1s1            5  Delete
 *      - b1c1          1  Reparent b1 parent
 *      - b1c2          4  Reparent b1 parent
 *      - b1c2s1        2
 *      - b1c2s2        3
 *    - b1s2            6  Delete
 *    - b2              12 Reparent a parent
 *      - b2c1          8
 *      - b2c2          11
 *      - b2c2s1        9
 *      - b2c2s2        10
 *    - b3              13 Reparent a parent
 *
 * d) Reparent subregions and synonyms
 *  - a                 14 Delete
 *    - b1              7  Reparent a parent
 *    - b1s1            5
 *      - b1c1          1
 *      - b1c2          4
 *      - b1c2s1        2
 *      - b1c2s2        3
 *    - b1s2            6
 *    - b2              12 Reparent a parent
 *      - b2c1          8
 *      - b2c2          11
 *      - b2c2s1        9
 *      - b2c2s2        10
 *    - b3              13 Reparent a parent
 *
 */
function countries_regions_region_delete(&$region, $delete_subregions = TRUE, $delete_synonyms = TRUE, $flush = TRUE) {
  // Subregions should not include synonyms.
  if ($subregions = countries_regions_tree($region->iso2, $region)) {
    foreach ($subregions as $child_region) {
      if ($delete_subregions) {
        countries_regions_region_delete($child_region, $delete_subregions, $delete_synonyms, FALSE);
      }
      else {
        $child_region->prid = $region->prid;
        countries_regions_region_save($child_region, FALSE);
      }
    }
  }

  if ($delete_synonyms) {
    $base_region = countries_regions_base_synonym($region);
    if ($synonyms = countries_regions_synonyms($region)) {
      foreach ($synonyms as $synonym) {
        // Take care not to recursively delete a region.
        if ($synonym->rid == $region->rid) {
          continue;
        }
        // Unless deleting child regions, stop deleting child synonyms
        if (!$delete_subregions) {
          $delete_synonyms = FALSE;
        }
        countries_regions_region_delete($synonym, $delete_subregions, $delete_synonyms, FALSE);
      }
    }
    if ($base_region->rid != $region->rid) {
      // We have a different base region, so delete this as well.
      // Do not delete the regions subregions, nor symonyms as this was done here.
      countries_regions_region_delete($base_region, $delete_subregions, FALSE, FALSE);
    }
  }
  else {
    // We only need to reparent if this is the main synonym.
    if ($synonyms = countries_regions_synonyms($region, FALSE)) {
      $new_base = array_shift($synonyms);
      $new_base->synonym = 0;
      countries_regions_region_save($new_base, FALSE);
      foreach ($synonyms as $synonym) {
        $synonym->synonym = $new_base->rid;
        countries_regions_region_save($synonym, FALSE);
      }
    }
  }

  field_attach_delete('country_region', $region);

  db_delete('countries_region')
    ->condition('rid', $region->rid)
    ->execute();

  module_invoke_all('countries_region_delete', $region);

  if ($flush) {
    field_cache_clear();
  }
}

/**
 * Private helper function to see if there is a regional type update required.
 */
function _countries_regions_type_update_required($region, $parent, $settings = NULL, $check_all = FALSE) {
  if (!isset($settings)) {
    $settings = countries_configuration($region->iso2, 'countries_regions');
  }

  $updates = array();

  // The region has a regional type override. There is no reason to recurse into
  // the child tree, nor to update its regional type. However, this function can
  // be used to check the entire tree, so recurse if requested.
  if (!$check_all) {
    return $updates;
  }

  if (empty($region->type_selector)) {
    $inherited_type = _countries_regions_regional_type_from_parent($parent, $settings['regions']);
    if ($inherited_type != $region->type) {
      $updates[$region->rid] = $inherited_type;
      $region->type = $inherited_type;
    }
  }
  if (!empty($region->subregions)) {
    foreach ($region->subregions as $subregion) {
      $updates += _countries_regions_type_update_required($subregion, $region, $settings, $check_all);
    }
  }
  return $updates;
}

/**
 * Private helper function.
 */
function _countries_regions_regional_type_from_parent($parent, $regional_settings) {
  $max_loops = COUNTRIES_REGIONS_MAX_DEPTH;
  while ($parent && $parent->type > 100 && $max_loops) {
    $max_loops--;
    $parent = $parent->prid ? countries_regions_load($parent->prid) : NULL;
  }
  if ($parent) {
    foreach ($regional_settings as $values) {
      if ($values['type'] > $parent->type) {
        return $values['type'];
      }
    }
    // We can not find child types, return the parental type.
    return $parent->type;
  }

  // If no parent was given, return the top level for the country.
  $top = array_shift($regional_settings);
  return empty($top['type']) ? COUNTRIES_REGIONS_C3 : $top['type'];
}

